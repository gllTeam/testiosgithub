//
//  RepoItenManager.swift
//  TestiOSGithub
//
//  Created by Gustavo Luís Soré on 03/03/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import Foundation

struct RepoItem {
    
    let name: String
    let description: String
    let forks: Int
    let stars: Int
    let pullsURL: String
    
    let userImage: String
    let userName: String
    
}

extension RepoItem{
    
    enum RepoItemError: Error {
        
        case jsonNotHaveAllNeededKeys
        
    }
    
    static func load (dictionary: Dictionary<String,Any>) throws -> RepoItem {
        
        
        if let name = dictionary["name"] as? String,
            let description = dictionary["description"] as? String,
            let forks = dictionary["forks"] as? Int,
            let stars = dictionary["watchers"] as? Int,
            let pullsURL = dictionary["pulls_url"] as? String,
            let owner = dictionary["owner"] as? Dictionary<String,Any>,
            let userName = owner["login"] as? String,
            let userImage = owner["avatar_url"] as? String{
            
            
            return RepoItem(
                name: name,
                description: description,
                forks: forks,
                stars: stars,
                pullsURL: pullsURL,
                userImage: userImage,
                userName: userName
            )
            
        } else{
            
            throw RepoItemError.jsonNotHaveAllNeededKeys
            
        }
        
        
    }

    
}

class RepoItemManager {
    
    static func downloadRepos (urlString: String,page: Int, completion: @escaping (_ result: Array<RepoItem>) -> Void){
        
        let url = URL(string: "\(urlString)\(page)")
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,Any>
            
            var arrayRepoItens:Array<RepoItem> = []
            
            let arrayItems:Array<Dictionary<String,Any>> = json["items"] as! Array<Dictionary<String,Any>>
            
            for i in 0...arrayItems.count - 1{
            
                do{
                    try arrayRepoItens.append(RepoItem.load(dictionary: arrayItems[i]))
                }
                catch RepoItem.RepoItemError.jsonNotHaveAllNeededKeys{
                    print("Json not have all nedded keys.")
                } catch{
                    
                }
                
            }
            
            completion(arrayRepoItens)
        }
        
        task.resume()
        
    }
    
}
