//
//  RepoTableViewController.swift
//  TestiOSGithub
//
//  Created by Gustavo Luís Soré on 03/03/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import UIKit
import SystemConfiguration
import SDWebImage

class RepoTableViewController: UITableViewController {
    
    var RepoItems:Array<RepoItem> = []
    var page:Int = 1
    var currentPullsURL:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if isInternetAvailable() == false{
            let alert = UIAlertController(title: "Atenção", message: "Sem internet.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok",
                                          style: UIAlertActionStyle.default,
                                          handler: {
                                            (alert: UIAlertAction!) in print("Sem internet.")
                                            
                                            
            }))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        } else{
            
            self.downloadItems()
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        SDImageCache.shared().clearMemory()
    }
    
    func downloadItems(){
        RepoItemManager.downloadRepos(urlString: "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=",
                                      page: page,
                                      completion: {
                                        (_ result: Array<RepoItem>) in
                                        self.RepoItems.append(contentsOf: result)
                                        DispatchQueue.main.async {
                                             self.tableView.reloadData()
                                        }
                                        SDImageCache.shared().clearMemory()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.RepoItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell", for: indexPath) as! RepoTableViewCell
        
        if indexPath.row >= ((page * 30) - 2){
            page = page + 1
            self.downloadItems()
        }
        
        let item:RepoItem = RepoItems[indexPath.row]
        
        cell.repoNameLabel.text = item.name
        cell.repoDescriptionLabel.text = item.description
        cell.forkLabel.text = "\(item.forks)"
        cell.starLabel.text = "\(item.stars)"
        cell.userLabel.text = item.userName
        
        cell.userImageView.sd_setImage(with: URL.init(string: item.userImage), placeholderImage: #imageLiteral(resourceName: "user"))

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128;
    }
    
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pullsSegue"{
            
            let pullTableViewController:PullTableViewController = segue.destination as! PullTableViewController
            let item:RepoItem = RepoItems[(self.tableView.indexPath(for: sender as! UITableViewCell)?.row)!]
            pullTableViewController.currentRepoPullsURLString = item.pullsURL
            pullTableViewController.navigationItem.title = item.name
            
        }
    }
    
    func isInternetAvailable() -> Bool
    {
        //Source: http://stackoverflow.com/questions/39558868/check-internet-connection-ios-10
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }

}
