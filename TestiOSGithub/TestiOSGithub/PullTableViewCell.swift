//
//  PullTableViewCell.swift
//  TestiOSGithub
//
//  Created by Gustavo Luís Soré on 03/03/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import UIKit

class PullTableViewCell: UITableViewCell {
    @IBOutlet var pullTitleLabel: UILabel!
    @IBOutlet var pullDescriptionLabel: UILabel!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
