//
//  PullItemManager.swift
//  TestiOSGithub
//
//  Created by Gustavo Luís Soré on 03/03/17.
//  Copyright © 2017 Sore. All rights reserved.
//

import Foundation

struct PullItem {
    
    let title   : String
    let body: String
    let htmlURL: String
    
    let userImage: String
    let userName: String
    
}

extension PullItem{
    
    enum PullItemError: Error {
        
        case jsonNotHaveAllNeededKeys
        
    }
    
    static func load (dictionary: Dictionary<String,Any>) throws -> PullItem {
        
        
        if let title = dictionary["title"] as? String,
            let body = dictionary["body"] as? String,
            let htmlURL = dictionary["html_url"] as? String,
            let user = dictionary["user"] as? Dictionary<String,Any>,
            let userName = user["login"] as? String,
            let userImage = user["avatar_url"] as? String{
            
            
            return PullItem(
                title: title,
                body: body,
                htmlURL: htmlURL,
                userImage: userImage,
                userName: userName
            )
            
        } else{
            
            throw PullItemError.jsonNotHaveAllNeededKeys
            
        }
        
        
    }
    
    
}

class PullItemManager {
    
    static func downloadPulls (urlString: String, completion: @escaping (_ result: Array<PullItem>) -> Void){
        
        let string = urlString.replacingOccurrences(of: "{/number}", with: "")
        let url = URL(string: string)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let json = try! JSONSerialization.jsonObject(with: data, options: []) as! Array<Dictionary<String,Any>>
            
            var arrayPullItens:Array<PullItem> = []
            
            if(json.count > 0){
                for i in 0...json.count - 1{
                    
                    do{
                        try arrayPullItens.append(PullItem.load(dictionary: json[i]))
                    }
                    catch RepoItem.RepoItemError.jsonNotHaveAllNeededKeys{
                        print("Json not have all nedded keys.")
                    } catch{
                        
                    }
                    
                }
            }
            
            completion(arrayPullItens)
        }
        
        task.resume()
        
    }
    
}




